/*
 * sensor.c
 *
 *  Created on: 15 mars 2022
 *      Author: chrispoue
 */
#include <msp430.h>
#include <sensor.h>
int miliseconds;
int dis;
long sensor;

void sensors(void)
{

  BCSCTL1 = CALBC1_1MHZ;
  DCOCTL = CALDCO_1MHZ;                     // submainclock 1mhz
  WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT

  CCTL0 = CCIE;                             // CCR0 interrupt enabled
  CCR0 = 1000;                  // 1ms at 1mhz
  TACTL = TASSEL_2 + MC_1;                  // SMCLK, upmode

  P1IFG  = 0x00;                //clear all interrupt flags
  P1DIR |= BIT0;
  P1OUT &= ~BIT0;                           // turn LED off

  _BIS_SR(GIE);                         // global interrupt enable


}


void distance(void){

    P1IE &= ~0x01;          // disable interupt
    P1DIR |= 0x02;          // trigger pin as output
    P1OUT |= 0x02;          // generate pulse
    __delay_cycles(10);             // for 10us
    P1OUT &= ~0x02;                 // stop pulse
    P1DIR &= ~0x04;         // make pin P1.2 input (ECHO)
        P1IFG = 0x00;                   // clear flag just in case anything happened before
    P1IE |= 0x04;           // enable interupt on ECHO pin
    P1IES &= ~0x04;         // rising edge on ECHO pin
    __delay_cycles(30000);          // delay for 30ms (after this time echo times out if there is no object detected)
    dis = sensor/58;           // converting ECHO length into cm
    if(dis < 20 && dis != 0){
        P1OUT |= BIT2;  //turning LED on if distance is less than 20cm and if distance isn't 0.
    }
    else{
        P1OUT &= ~BIT2;
    }
}


