#include <msp430.h>
#include <servomotor.h>


int i;
void motors(void)
{

    BCSCTL1= CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
    //PWM period
    P1DIR |= BIT2;
    P1SEL |= BIT2;  //selection for timer setting
    while(1) {
    TACCR0 = 20000;  //PWM period
    for (i=350; i<=2350; i++) {
        TACCR1 = i;
        TACCTL1 = OUTMOD_7;  //CCR1 selection reset-set
        TACTL = TASSEL_2|MC_1;   //SMCLK submain clock,upmode
        __delay_cycles(1000);
    }
    __delay_cycles(1000000);
    }
}
